\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}User Guide}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Quickstart}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Install PyScribus}{3}{subsection.1.1.1}%
\contentsline {subsubsection}{Pip}{3}{subsubsection*.3}%
\contentsline {subsubsection}{Git}{3}{subsubsection*.4}%
\contentsline {subsection}{\numberline {1.1.2}An introduction to PyScribus modules}{3}{subsection.1.1.2}%
\contentsline {subsubsection}{Used for parsing \& generating}{3}{subsubsection*.5}%
\contentsline {subsubsection}{Paper sizes}{3}{subsubsection*.6}%
\contentsline {subsubsection}{Extras}{4}{subsubsection*.7}%
\contentsline {subsubsection}{Basis}{4}{subsubsection*.8}%
\contentsline {subsection}{\numberline {1.1.3}Basics of PyScribus objects}{4}{subsection.1.1.3}%
\contentsline {subsubsection}{Defaults}{4}{subsubsection*.9}%
\contentsline {subsubsection}{XML}{4}{subsubsection*.10}%
\contentsline {subsection}{\numberline {1.1.4}Measures and geometry in PyScribus}{5}{subsection.1.1.4}%
\contentsline {subsection}{\numberline {1.1.5}Frequent tasks}{5}{subsection.1.1.5}%
\contentsline {subsubsection}{Reading an existing SLA file}{5}{subsubsection*.11}%
\contentsline {subsubsection}{Generating an SLA file}{5}{subsubsection*.12}%
\contentsline {subsubsection}{Creating a frame (page object)}{5}{subsubsection*.13}%
\contentsline {subsubsection}{Adding things to a SLA file}{5}{subsubsection*.14}%
\contentsline {section}{\numberline {1.2}PyScribus Story Markup}{5}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Main elements}{5}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Span element}{5}{subsection.1.2.2}%
\contentsline {subsubsection}{@style}{5}{subsubsection*.15}%
\contentsline {subsubsection}{@class}{6}{subsubsection*.16}%
\contentsline {section}{\numberline {1.3}Templating}{6}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}What you can do with PyScribus templating}{6}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Activation \& configuration}{6}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Load data}{6}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Logging}{7}{section.1.4}%
\contentsline {subsection}{\numberline {1.4.1}Activation}{7}{subsection.1.4.1}%
\contentsline {subsection}{\numberline {1.4.2}Configuration}{7}{subsection.1.4.2}%
\contentsline {section}{\numberline {1.5}Examples}{7}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Draw a wireframe of a file}{7}{subsection.1.5.1}%
\contentsline {section}{\numberline {1.6}Scribus SLA for humans}{8}{section.1.6}%
\contentsline {subsection}{\numberline {1.6.1}{[}FR{]} Comment les \sphinxstyleemphasis {stories} de Scribus (semblent) fonctionner}{8}{subsection.1.6.1}%
\contentsline {subsubsection}{Comment faire un paragraphe}{8}{subsubsection*.17}%
\contentsline {subsection}{\numberline {1.6.2}{[}FR{]} Mesures et autres nombres}{8}{subsection.1.6.2}%
\contentsline {chapter}{\numberline {2}Documentation}{9}{chapter.2}%
\contentsline {section}{\numberline {2.1}Reference}{9}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}pyscribus package}{9}{subsection.2.1.1}%
\contentsline {subsubsection}{Basis}{9}{subsubsection*.18}%
\contentsline {paragraph}{pyscribus.common.xml}{9}{paragraph*.19}%
\contentsline {paragraph}{pyscribus.common.math}{11}{paragraph*.40}%
\contentsline {paragraph}{pyscribus.exceptions}{12}{paragraph*.45}%
\contentsline {subsubsection}{Usual}{12}{subsubsection*.59}%
\contentsline {paragraph}{pyscribus.dimensions}{12}{paragraph*.60}%
\contentsline {paragraph}{pyscribus.document}{16}{paragraph*.86}%
\contentsline {paragraph}{pyscribus.layers}{17}{paragraph*.107}%
\contentsline {paragraph}{pyscribus.colors}{18}{paragraph*.113}%
\contentsline {paragraph}{pyscribus.patterns}{20}{paragraph*.134}%
\contentsline {paragraph}{pyscribus.pageobjects}{20}{paragraph*.141}%
\contentsline {paragraph}{pyscribus.pages}{29}{paragraph*.224}%
\contentsline {paragraph}{pyscribus.sla}{31}{paragraph*.254}%
\contentsline {paragraph}{pyscribus.stories}{33}{paragraph*.265}%
\contentsline {paragraph}{pyscribus.notes}{37}{paragraph*.317}%
\contentsline {paragraph}{pyscribus.marks}{38}{paragraph*.333}%
\contentsline {paragraph}{pyscribus.styles}{40}{paragraph*.351}%
\contentsline {paragraph}{pyscribus.toc}{44}{paragraph*.419}%
\contentsline {paragraph}{pyscribus.printing}{45}{paragraph*.429}%
\contentsline {paragraph}{pyscribus.itemattribute}{46}{paragraph*.445}%
\contentsline {subsubsection}{Headless}{47}{subsubsection*.454}%
\contentsline {paragraph}{Script: topdf}{47}{paragraph*.456}%
\contentsline {subsubsection}{Paper sizes}{47}{subsubsection*.457}%
\contentsline {paragraph}{pyscribus.papers.iso216}{47}{paragraph*.458}%
\contentsline {paragraph}{pyscribus.papers.iso269}{49}{paragraph*.525}%
\contentsline {paragraph}{pyscribus.papers.iso217}{50}{paragraph*.565}%
\contentsline {paragraph}{pyscribus.papers.afnor}{50}{paragraph*.566}%
\contentsline {paragraph}{pyscribus.papers.ansi}{52}{paragraph*.639}%
\contentsline {subsection}{\numberline {2.1.2}Extras packages}{53}{subsection.2.1.2}%
\contentsline {subsubsection}{pyscribus.extra.wireframe}{53}{subsubsection*.658}%
\contentsline {chapter}{\numberline {3}Scribus SLA specification improvements}{55}{chapter.3}%
\contentsline {section}{\numberline {3.1}{[}EN{]} SLA document\sphinxhyphen {}able attributes}{55}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}DOCUMENT}{55}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}DOCUMENT/LAYERS}{55}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}DOCUMENT/CHARSTYLE}{55}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}DOCUMENT/Sections}{55}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}DOCUMENT/PAGEOBJECT}{55}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Notes}{56}{subsection.3.1.6}%
\contentsline {subsection}{\numberline {3.1.7}Corrections}{56}{subsection.3.1.7}%
\contentsline {subsection}{\numberline {3.1.8}Notable things}{56}{subsection.3.1.8}%
\contentsline {section}{\numberline {3.2}{[}EN{]} SLA’s enhancement suggestions}{56}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}\# 1}{56}{subsection.3.2.1}%
\contentsline {subsubsection}{Discussion}{56}{subsubsection*.667}%
\contentsline {subsection}{\numberline {3.2.2}\# 2}{56}{subsection.3.2.2}%
\contentsline {subsubsection}{Discussion}{56}{subsubsection*.668}%
\contentsline {subsection}{\numberline {3.2.3}\# 3}{56}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}\# 4}{56}{subsection.3.2.4}%
\contentsline {subsubsection}{Discussion}{56}{subsubsection*.669}%
\contentsline {chapter}{\numberline {4}Appendix}{57}{chapter.4}%
\contentsline {section}{\numberline {4.1}Credits}{57}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Logo}{57}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}In example files}{57}{subsection.4.1.2}%
\contentsline {subsubsection}{Wireframe}{57}{subsubsection*.670}%
\contentsline {section}{\numberline {4.2}Acknowledgements}{57}{section.4.2}%
\contentsline {chapter}{\numberline {5}Indices and tables}{59}{chapter.5}%
\contentsline {chapter}{Python Module Index}{61}{section*.671}%
\contentsline {chapter}{Index}{63}{section*.672}%
